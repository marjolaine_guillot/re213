var mysql = require('mysql');

function pool() {
    return mysql.createPool({
        connectionLimit: 100, //important
        multipleStatements: true,
        host: 'database',
        port: '3306',
        user: 'dbuser',
        password: 'user',
        database: 'database',
        insecureAuth: true
    });
}

exports.queryRequest = function (query, data) {
    return new Promise(function (resolve, reject) {
        pool().query(query, data, function (err, rows) {
            if (err) {
                return reject(err);
            }
            resolve(rows);
        });
    })
};
