var db = require('./dbUtils');

exports.getAllGroups = function () {
    return db.queryRequest("SELECT name, creation_date, username as creator FROM \`Group`\ INNER JOIN \`User\` ON Group.creator = User.idUser");
};

exports.createGroup = function (groupName, creator, members) {
    let data = [groupName, creator];
    let query = "INSERT INTO \`Group\` (name, creator) VALUES (?, (SELECT idUser FROM \`User\` WHERE username=?))";
    let req = db.queryRequest(query, data);
    return req.then(response => {
        if (typeof members !== 'undefined' && members.length > 0) {
            let membersData = [groupName, creator];
            let addMembersQuery = "INSERT INTO \`Member\` (Group_idGroup, User_idUser) VALUES ((SELECT idGroup FROM \`Group\` WHERE name= ?), (SELECT idUser FROM \`User\` WHERE username= ?)),";
            let i;
            for (i = 0; i < members.length; i++) {
                membersData.push(groupName, members[i]);
                addMembersQuery += " ((SELECT idGroup FROM \`Group\` WHERE name= ?), (SELECT idUser FROM \`User\` WHERE username= ?)),";
            }
            let q = addMembersQuery.replace(/.$/, ";");
            return db.queryRequest(q, membersData);
        } else {
            return response;
        }
    }, reject => {
        return reject;
    });
};

exports.getGroup = function (name) {
    return db.queryRequest("select * from \`Group\` where name=?", name);
};

exports.getGroupMembers = function (name) {
    return db.queryRequest("SELECT username FROM \`Group\` INNER JOIN \`Member\` ON Group.idGroup = Member.Group_idGroup INNER JOIN \`User\` ON Member.User_idUser = User.idUser where name=?", name);
};

exports.joinGroup = function (groupName, username) {
    let data = [groupName, username];
    let query = "INSERT INTO \`Member\` (Group_idGroup, User_idUser) VALUES ((SELECT idGroup FROM \`Group\` WHERE name=?), (SELECT idUser FROM \`User\` WHERE username=?))";
    return db.queryRequest(query, data);
};

//TODO Bogue si on supprime un groupe on doit pouvoir supprimer les messages du groupe
exports.deleteGroup = function (name) {
    return getGroupId(name).then(response => {
        let groupID = response[0].idGroup;
        let query = "DELETE FROM \`Message\` WHERE group_destination = ?; DELETE FROM \`Member\` WHERE Group_idGroup = ?; DELETE FROM \`Group\` WHERE name = ?;";
        let data = [groupID, groupID, name];
        return db.queryRequest(query, data);
    }, reject => {
        return reject;
    });
};

function getGroupId (name) {
    return db.queryRequest("SELECT idGroup FROM \`Group\` WHERE name = ?", name);
}
