'use strict';

var utils = require('../utils/writer.js');
var Message = require('../service/MessageService');

module.exports.messageGroupNameGET = function messageGroupNameGET (req, res, next, groupName) {
  Message.messageGroupNameGET(groupName)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.messageGroupNamePOST = function messageGroupNamePOST (req, res, next, message, sender, groupName) {
  Message.messageGroupNamePOST(message, sender, groupName)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.messagesGET = function messagesGET (req, res) {
  Message.messagesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
