'use strict';
var userDAL = require('../dal/UserDAL');

/**
 * Find all users.
 *
 * returns List
 **/
exports.usersGET = function() {
  return userDAL.getAllUsers();
};

/**
 * Create a user.
 *
 * username String the name of the user
 * password String
 * no response value expected for this operation
 **/
exports.userPOST = function(username,password) {
  return userDAL.createUser(username, password);
};


/**
 * Delete a user.
 *
 * username String
 * password String
 * no response value expected for this operation
 **/
exports.userDELETE = function(username,password) {
  return userDAL.deleteUser(username, password);
};

