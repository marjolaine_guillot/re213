'use strict';
var groupDAL = require('../dal/GroupDAL');

/**
 * Find all groups.
 * Optional extended description in CommonMark or HTML.
 *
 * returns List
 **/
exports.groupsGET = function() {
  return groupDAL.getAllGroups();
};

/**
 * Create a group.
 *
 * groupName String Parameter description in CommonMark or HTML.
 * creator String the creator of the group
 * members List  (optional)
 * no response value expected for this operation
 **/
exports.groupGroupNamePOST = function(groupName,creator,members) {
  return groupDAL.createGroup(groupName, creator, members);
};

/**
 * Join a group.
 *
 * groupName String Parameter description in CommonMark or HTML.
 * username String the user who wants to join the group
 * no response value expected for this operation
 **/
exports.groupGroupNamePUT = function(groupName,username) {
  return groupDAL.joinGroup(groupName, username);
};

/**
 * Get a group members.
 *
 * groupName String Parameter description in CommonMark or HTML.
 * no response value expected for this operation
 **/
exports.groupGroupNameGET = function(groupName) {
  return groupDAL.getGroupMembers(groupName);
};

/**
 * Delete a group.
 *
 * groupName String Parameter description in CommonMark or HTML.
 * no response value expected for this operation
 **/
exports.groupGroupNameDELETE = function(groupName) {
  return groupDAL.deleteGroup(groupName);
};
