var db = require('./dbUtils');

exports.getAllMessages = function () {
    return db.queryRequest("select * from \`Message\`");
};

exports.getGroupMessages = function (groupName) {
    return db.queryRequest("select * from \`Message\` INNER JOIN \`Group\` on Message.group_destination = Group.idGroup where name = ?", groupName);
};

exports.createMessage = function (message, sender, groupName) {
    let data = [message, "text", sender, groupName];
    let query = "INSERT INTO \`Message\` (content, type, sender, group_destination) VALUES (?, ?, (SELECT idUser FROM \`User\` WHERE username=?), (SELECT idGroup FROM \`Group\` WHERE name=?))";
    return db.queryRequest(query, data);
};
