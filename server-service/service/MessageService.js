'use strict';
var messageDAL = require('../dal/MessageDAL');

/**
 * Find all messages.
 * Optional extended description in CommonMark or HTML.
 *
 * returns List
 **/
exports.messagesGET = function() {
  return messageDAL.getAllMessages();
};

/**
 * Find all the messages of a group
 *
 * groupName String Parameter description in CommonMark or HTML.
 * no response value expected for this operation
 **/
exports.messageGroupNameGET = function(groupName) {
  return messageDAL.getGroupMessages(groupName);
};

/**
 * Send a message to a group.
 *
 * message String the message to be sent
 * sender String the sender of the message
 * groupName String the group receiver
 * no response value expected for this operation
 **/
exports.messageGroupNamePOST = function(message,sender,groupName) {
  return messageDAL.createMessage(message, sender, groupName);
};

