'use strict';

var utils = require('../utils/writer.js');
var Group = require('../service/GroupService');

module.exports.groupGroupNameDELETE = function groupGroupNameDELETE (req, res, next, groupName) {
  Group.groupGroupNameDELETE(groupName)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.groupGroupNamePOST = function groupGroupNamePOST (req, res, next,  creator, members, groupName) {
    Group.groupGroupNamePOST(groupName, creator, members)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.groupGroupNamePUT = function groupGroupNamePUT (req, res, next, username, groupName) {
    console.log("PARAMETERS ARE: groupname and username", groupName, username);
    Group.groupGroupNamePUT(groupName, username)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.groupGroupNameGET = function groupGroupNameGET (req, res, next, groupName) {
    Group.groupGroupNameGET(groupName)
        .then(function (response) {
            utils.writeJson(res, response);
        })
        .catch(function (response) {
            utils.writeJson(res, response);
        });
};


module.exports.groupsGET = function groupsGET (req, res) {
  Group.groupsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
