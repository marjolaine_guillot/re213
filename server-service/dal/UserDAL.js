var db = require('./dbUtils');

exports.getAllUsers = function () {
    return db.queryRequest("select * from User");
};

exports.getUser = function (username) {
    return db.queryRequest("select * from \`User\` where username=?", username);
};

exports.getUserID = function (username) {
    return db.queryRequest("select idUser from \`User\` where username=?", username);
};

exports.modifyUser = function (username, password) {
    let query = "UPDATE \`User\` SET password = ? WHERE username = ?";
    let data = [password, username];
    return db.queryRequest(query, data);
};

exports.createUser = function (username, password) {
    let query = "INSERT INTO \`User\`(username,password) VALUES(?,?)";
    let data = [username, password];
    return db.queryRequest(query, data);
};

exports.deleteUser = function (username, password) {
    let data = [username, password];
    return db.queryRequest("DELETE FROM \`User\` WHERE username = ? AND password=?", data);
};
